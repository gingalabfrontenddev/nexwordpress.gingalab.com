<div class="edgtf-section-title-holder <?php echo esc_attr( $holder_classes ); ?>" <?php echo orkan_edge_get_inline_style( $holder_styles ); ?>>
	<div class="edgtf-st-inner">
		<?php if ( ! empty( $title ) ) { ?>
			<<?php echo esc_attr( $title_tag ); ?> class="edgtf-st-title" <?php echo orkan_edge_get_inline_style( $title_styles ); ?>>
				<?php echo wp_kses( $title, array( 'br' => true, 'span' => array( 'class' => true, 'style' => true ) ) ); ?>
			</<?php echo esc_attr( $title_tag ); ?>>
		<?php } ?>
		<?php if ( $enable_separator === 'yes') { ?>
			<div class="edgtf-st-separator-holder" <?php echo orkan_edge_get_inline_style( $separator_holder_styles ); ?>>
				<span class="edgtf-st-separator" <?php echo orkan_edge_get_inline_style( $separator_styles ); ?>></span>
			</div>
		<?php } ?>
		<?php if ( ! empty( $text ) ) { ?>
			<<?php echo esc_attr( $text_tag ); ?> class="edgtf-st-text" <?php echo orkan_edge_get_inline_style( $text_styles ); ?>>
				<?php echo wp_kses( $text, array( 'br' => true ) ); ?>
			</<?php echo esc_attr( $text_tag ); ?>>
		<?php } ?>
		<?php if ( ! empty( $link ) && ! empty( $link_text ) ) {
			$button_params = array(
				'link'                   => $link,
				'target'                 => $target,
				'text'                   => $link_text,
                'hover_animation'        => 'yes',
				'custom_class'           => 'edgtf-st-button',
                'skin'                   => 'blue-black'
//				'color'                  => $link_color,
//				'hover_color'            => $link_hover_color,
//				'background_color'       => $link_bg_color,
//				'hover_background_color' => $link_hover_bg_color
			);
			
			echo orkan_edge_get_button_html( $button_params );
		} ?>
	</div>
</div>