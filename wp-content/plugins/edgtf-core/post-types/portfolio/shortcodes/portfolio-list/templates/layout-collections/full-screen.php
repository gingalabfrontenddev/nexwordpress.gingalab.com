<?php
$image_src   = get_the_post_thumbnail_url( get_the_ID() );
$image_style = ! empty( $image_src ) ? 'background-image:url(' . esc_url( $image_src ) . ')' : '';
$excerpt     = get_the_excerpt() !== '' ? substr( get_the_excerpt(), 0, 120 ) : '';
$categories  = wp_get_post_terms(get_the_ID(), 'portfolio-category');
$tags        = wp_get_post_terms(get_the_ID(), 'portfolio-tag');
$share_on    = orkan_edge_options()->getOptionValue('enable_social_share') == 'yes' && orkan_edge_options()->getOptionValue('enable_social_share_on_portfolio-item') == 'yes';
?>
<div class="edgtf-pli-image" <?php orkan_edge_inline_style($image_style); ?>></div>
<div class="edgtf-pli-text-holder">
	<div class="edgtf-pli-text-wrapper">
		<div class="edgtf-pli-text">
			<div class="edgtf-pli-text-inner">
				<a class="edgtf-pli-up-arrow" href="#"><i class="fa fa fa-chevron-up"></i></a>
				<h2 itemprop="name" class="edgtf-pli-title entry-title"><a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php echo esc_attr( get_the_title() ); ?></a></h2>
				
				<div class="edgtf-pli-date"><?php the_time( get_option( 'date_format' ) ); ?></div>
				
				<div class="edgtf-pli-info-holder">
					<?php if ( ! empty( $excerpt ) ) { ?>
						<p itemprop="description" class="edgtf-pli-excerpt"><?php echo esc_html( $excerpt ); ?></p>
					<?php } ?>
					
					<?php if ( ! empty( $categories ) ) { ?>
						<div class="edgtf-pli-category-info edgtf-pli-info">
							<h5 class="edgtf-pli-info-title"><?php esc_html_e( 'Category:', 'orkan' ); ?></h5>
							<p>
								<?php foreach ( $categories as $cat ) { ?>
									<a itemprop="url" href="<?php echo esc_url( get_term_link( $cat->term_id ) ); ?>"><?php echo esc_html( $cat->name ); ?></a>
								<?php } ?>
							</p>
						</div>
					<?php } ?>
					
					<div class="edgtf-pli-date-info edgtf-pli-info">
						<h5 class="edgtf-pli-info-title"><?php esc_html_e( 'Date:', 'orkan' ); ?></h5>
						<p><?php the_time( get_option( 'date_format' ) ); ?></p>
					</div>
					
					<?php if ( ! empty( $tags ) ) { ?>
						<div class="edgtf-pli-tag-info edgtf-pli-info">
							<h5 class="edgtf-pli-info-title"><?php esc_html_e( 'Tag:', 'orkan' ); ?></h5>
							<p>
								<?php foreach ( $tags as $tag ) { ?>
									<a itemprop="url" href="<?php echo esc_url( get_term_link( $tag->term_id ) ); ?>"><?php echo esc_html( $tag->name ); ?></a>
								<?php } ?>
							</p>
						</div>
					<?php } ?>
					
					<?php if ( $share_on ) { ?>
						<div class="edgtf-pli-share-info edgtf-pli-info">
							<h4 class="edgtf-pli-share-title"><?php esc_html_e( 'Share', 'orkan' ); ?></h4>
							<?php echo orkan_edge_get_social_share_html() ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>