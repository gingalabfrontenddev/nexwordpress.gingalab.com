<?php if(orkan_edge_options()->getOptionValue('enable_social_share') == 'yes' && orkan_edge_options()->getOptionValue('enable_social_share_on_portfolio-item') == 'yes') : ?>
    <div class="edgtf-ps-info-item edgtf-ps-social-share">
        <?php echo orkan_edge_get_social_share_html( array( 'title' => esc_html__( 'Share', 'orkan' ) ) ) ?>
    </div>
<?php endif; ?>