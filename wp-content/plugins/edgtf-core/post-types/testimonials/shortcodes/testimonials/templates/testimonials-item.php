<div class="edgtf-testimonial-content" id="edgtf-testimonials-<?php echo esc_attr( $current_id ) ?>">
	<?php if ( ! empty( $title ) ) { ?>
		<h2 itemprop="name" class="edgtf-testimonial-title entry-title"><?php echo esc_html( $title ); ?></h2>
	<?php } ?>
	<?php if ( ! empty( $text ) ) { ?>
		<h3 class="edgtf-testimonial-text"><?php echo esc_html( $text ); ?></h3>
	<?php } ?>
	<?php if ( ! empty( $author ) ) { ?>
		<h5 class="edgtf-testimonial-author"><?php echo esc_html( $author ); ?></h5>
	<?php } ?>
</div>