<div class="edgtf-side-menu">
	<a class="edgtf-close-side-menu" href="#">
		<?php echo orkan_edge_icon_collections()->renderIcon( 'icon_close', 'font_elegant' ); ?>
	</a>
	<?php if ( is_active_sidebar( 'sidearea' ) ) {
		dynamic_sidebar( 'sidearea' );
	} ?>
</div>