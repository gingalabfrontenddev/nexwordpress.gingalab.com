<?php

if ( ! function_exists( 'orkan_edge_get_hide_dep_for_header_standard_options' ) ) {
	function orkan_edge_get_hide_dep_for_header_standard_options() {
		$hide_dep_options = apply_filters( 'orkan_edge_header_standard_hide_global_option', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'orkan_edge_header_standard_map' ) ) {
	function orkan_edge_header_standard_map( $parent ) {
		$hide_dep_options = orkan_edge_get_hide_dep_for_header_standard_options();
		
		orkan_edge_add_admin_field(
			array(
				'parent'          => $parent,
				'type'            => 'select',
				'name'            => 'set_menu_area_position',
				'default_value'   => 'right',
				'label'           => esc_html__( 'Choose Menu Area Position', 'orkan' ),
				'description'     => esc_html__( 'Select menu area position in your header', 'orkan' ),
				'options'         => array(
					'right'  => esc_html__( 'Right', 'orkan' ),
					'left'   => esc_html__( 'Left', 'orkan' ),
					'center' => esc_html__( 'Center', 'orkan' )
				),
				'dependency' => array(
					'hide' => array(
						'header_options'  => $hide_dep_options
					)
				)
			)
		);
	}
	
	add_action( 'orkan_edge_additional_header_menu_area_options_map', 'orkan_edge_header_standard_map' );
}