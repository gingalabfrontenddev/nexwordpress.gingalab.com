<?php

if ( ! function_exists( 'orkan_edge_header_minimal_full_screen_menu_body_class' ) ) {
	/**
	 * Function that adds body classes for different full screen menu types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function orkan_edge_header_minimal_full_screen_menu_body_class( $classes ) {
		$classes[] = 'edgtf-' . orkan_edge_options()->getOptionValue( 'fullscreen_menu_animation_style' );
		
		return $classes;
	}
	
	if ( orkan_edge_check_is_header_type_enabled( 'header-minimal', orkan_edge_get_page_id() ) ) {
		add_filter( 'body_class', 'orkan_edge_header_minimal_full_screen_menu_body_class' );
	}
}

if ( ! function_exists( 'orkan_edge_get_header_minimal_full_screen_menu' ) ) {
	/**
	 * Loads fullscreen menu HTML template
	 */
	function orkan_edge_get_header_minimal_full_screen_menu() {
		$parameters = array(
			'fullscreen_menu_in_grid' => orkan_edge_options()->getOptionValue( 'fullscreen_in_grid' ) === 'yes' ? true : false
		);
		
		orkan_edge_get_module_template_part( 'templates/full-screen-menu', 'header/types/header-minimal', '', $parameters );
	}
	
	if ( orkan_edge_check_is_header_type_enabled( 'header-minimal', orkan_edge_get_page_id() ) ) {
		add_action( 'orkan_edge_after_wrapper_inner', 'orkan_edge_get_header_minimal_full_screen_menu', 40 );
	}
}

if ( ! function_exists( 'orkan_edge_header_minimal_mobile_menu_module' ) ) {
    /**
     * Function that edits module for mobile menu
     *
     * @param $module - default module value
     *
     * @return string name of module
     */
    function orkan_edge_header_minimal_mobile_menu_module( $module ) {
        return 'header/types/header-minimal';
    }

    if ( orkan_edge_check_is_header_type_enabled( 'header-minimal', orkan_edge_get_page_id() ) ) {
        add_filter('orkan_edge_mobile_menu_module', 'orkan_edge_header_minimal_mobile_menu_module');
    }
}

if ( ! function_exists( 'orkan_edge_header_minimal_mobile_menu_slug' ) ) {
    /**
     * Function that edits slug for mobile menu
     *
     * @param $slug - default slug value
     *
     * @return string name of slug
     */
    function orkan_edge_header_minimal_mobile_menu_slug( $slug ) {
        return 'minimal';
    }

    if ( orkan_edge_check_is_header_type_enabled( 'header-minimal', orkan_edge_get_page_id() ) ) {
        add_filter('orkan_edge_mobile_menu_slug', 'orkan_edge_header_minimal_mobile_menu_slug');
    }
}