<?php

if ( ! function_exists( 'orkan_edge_get_hide_dep_for_header_vertical_area_options' ) ) {
	function orkan_edge_get_hide_dep_for_header_vertical_area_options() {
		$hide_dep_options = apply_filters( 'orkan_edge_header_vertical_hide_global_option', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'orkan_edge_header_vertical_options_map' ) ) {
	function orkan_edge_header_vertical_options_map( $panel_header ) {
		$hide_dep_options = orkan_edge_get_hide_dep_for_header_vertical_area_options();
		
		$vertical_area_container = orkan_edge_add_admin_container_no_style(
			array(
				'parent'          => $panel_header,
				'name'            => 'header_vertical_area_container',
				'dependency' => array(
					'hide' => array(
						'header_options'  => $hide_dep_options
					)
				)
			)
		);
		
		orkan_edge_add_admin_section_title(
			array(
				'parent' => $vertical_area_container,
				'name'   => 'menu_area_style',
				'title'  => esc_html__( 'Vertical Area Style', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'        => 'vertical_header_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'orkan' ),
				'description' => esc_html__( 'Set background color for vertical menu', 'orkan' ),
				'parent'      => $vertical_area_container
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'vertical_header_background_image',
				'type'          => 'image',
				'default_value' => '',
				'label'         => esc_html__( 'Background Image', 'orkan' ),
				'description'   => esc_html__( 'Set background image for vertical menu', 'orkan' ),
				'parent'        => $vertical_area_container
			)
		);
		
		do_action( 'orkan_edge_header_vertical_area_additional_options', $panel_header );
	}
	
	add_action( 'orkan_edge_additional_header_menu_area_options_map', 'orkan_edge_header_vertical_options_map' );
}