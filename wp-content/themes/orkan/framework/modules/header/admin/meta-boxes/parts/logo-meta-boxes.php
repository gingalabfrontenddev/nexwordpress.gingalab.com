<?php

if ( ! function_exists( 'orkan_edge_logo_meta_box_map' ) ) {
	function orkan_edge_logo_meta_box_map() {
		
		$logo_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => apply_filters( 'orkan_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'logo_meta' ),
				'title' => esc_html__( 'Logo', 'orkan' ),
				'name'  => 'logo_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Default', 'orkan' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'orkan' ),
				'parent'      => $logo_meta_box
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_dark_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Dark', 'orkan' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'orkan' ),
				'parent'      => $logo_meta_box
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_light_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Light', 'orkan' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'orkan' ),
				'parent'      => $logo_meta_box
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_sticky_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Sticky', 'orkan' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'orkan' ),
				'parent'      => $logo_meta_box
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_mobile_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Mobile', 'orkan' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'orkan' ),
				'parent'      => $logo_meta_box
			)
		);
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_logo_meta_box_map', 47 );
}