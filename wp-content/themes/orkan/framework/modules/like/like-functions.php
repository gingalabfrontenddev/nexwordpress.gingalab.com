<?php

if ( ! function_exists( 'orkan_edge_like' ) ) {
	/**
	 * Returns OrkanEdgeClassLike instance
	 *
	 * @return OrkanEdgeClassLike
	 */
	function orkan_edge_like() {
		return OrkanEdgeClassLike::get_instance();
	}
}

function orkan_edge_get_like() {
	
	echo wp_kses( orkan_edge_like()->add_like(), array(
		'span' => array(
			'class'       => true,
			'aria-hidden' => true,
			'style'       => true,
			'id'          => true
		),
		'i'    => array(
			'class' => true,
			'style' => true,
			'id'    => true
		),
		'a'    => array(
			'href'  => true,
			'class' => true,
			'id'    => true,
			'title' => true,
			'style' => true
		)
	) );
}