<?php

if ( ! function_exists( 'orkan_edge_footer_options_map' ) ) {
	function orkan_edge_footer_options_map() {

		orkan_edge_add_admin_page(
			array(
				'slug'  => '_footer_page',
				'title' => esc_html__( 'Footer', 'orkan' ),
				'icon'  => 'fa fa-sort-amount-asc'
			)
		);

		$footer_panel = orkan_edge_add_admin_panel(
			array(
				'title' => esc_html__( 'Footer', 'orkan' ),
				'name'  => 'footer',
				'page'  => '_footer_page'
			)
		);

		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'footer_in_grid',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Footer in Grid', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will place Footer content in grid', 'orkan' ),
				'parent'        => $footer_panel
			)
		);

		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_top',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Footer Top', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'orkan' ),
				'parent'        => $footer_panel,
			)
		);
		
		$show_footer_top_container = orkan_edge_add_admin_container(
			array(
				'name'       => 'show_footer_top_container',
				'parent'     => $footer_panel,
				'dependency' => array(
					'show' => array(
						'show_footer_top' => 'yes'
					)
				)
			)
		);

		orkan_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_top_columns',
				'parent'        => $show_footer_top_container,
				'default_value' => '4',
				'label'         => esc_html__( 'Footer Top Columns', 'orkan' ),
				'description'   => esc_html__( 'Choose number of columns for Footer Top area', 'orkan' ),
				'options'       => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4'
				)
			)
		);

		orkan_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_top_columns_alignment',
				'default_value' => 'left',
				'label'         => esc_html__( 'Footer Top Columns Alignment', 'orkan' ),
				'description'   => esc_html__( 'Text Alignment in Footer Columns', 'orkan' ),
				'options'       => array(
					''       => esc_html__( 'Default', 'orkan' ),
					'left'   => esc_html__( 'Left', 'orkan' ),
					'center' => esc_html__( 'Center', 'orkan' ),
					'right'  => esc_html__( 'Right', 'orkan' )
				),
				'parent'        => $show_footer_top_container,
			)
		);

		orkan_edge_add_admin_field(
			array(
				'name'        => 'footer_top_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'orkan' ),
				'description' => esc_html__( 'Set background color for top footer area', 'orkan' ),
				'parent'      => $show_footer_top_container
			)
		);

		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_bottom',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Footer Bottom', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'orkan' ),
				'parent'        => $footer_panel,
			)
		);

		$show_footer_bottom_container = orkan_edge_add_admin_container(
			array(
				'name'            => 'show_footer_bottom_container',
				'parent'          => $footer_panel,
				'dependency' => array(
					'show' => array(
						'show_footer_bottom'  => 'yes'
					)
				)
			)
		);

		orkan_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_bottom_columns',
				'default_value' => '2',
				'label'         => esc_html__( 'Footer Bottom Columns', 'orkan' ),
				'description'   => esc_html__( 'Choose number of columns for Footer Bottom area', 'orkan' ),
				'options'       => array(
					'1' => '1',
					'2' => '2',
					'3' => '3'
				),
				'parent'        => $show_footer_bottom_container,
			)
		);

		orkan_edge_add_admin_field(
			array(
				'name'        => 'footer_bottom_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'orkan' ),
				'description' => esc_html__( 'Set background color for bottom footer area', 'orkan' ),
				'parent'      => $show_footer_bottom_container
			)
		);
	}

	add_action( 'orkan_edge_options_map', 'orkan_edge_footer_options_map', 11 );
}