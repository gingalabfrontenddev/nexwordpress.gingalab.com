<?php

if ( ! function_exists( 'orkan_edge_get_title_types_options' ) ) {
	function orkan_edge_get_title_types_options() {
		$title_type_options = apply_filters( 'orkan_edge_title_type_global_option', $title_type_options = array() );
		
		return $title_type_options;
	}
}

if ( ! function_exists( 'orkan_edge_get_title_type_default_options' ) ) {
	function orkan_edge_get_title_type_default_options() {
		$title_type_option = apply_filters( 'orkan_edge_default_title_type_global_option', $title_type_option = '' );
		
		return $title_type_option;
	}
}

foreach ( glob( EDGE_FRAMEWORK_MODULES_ROOT_DIR . '/title/types/*/admin/options-map/*.php' ) as $options_load ) {
	include_once $options_load;
}

if ( ! function_exists('orkan_edge_title_options_map') ) {
	function orkan_edge_title_options_map() {
		$title_type_options        = orkan_edge_get_title_types_options();
		$title_type_default_option = orkan_edge_get_title_type_default_options();

		orkan_edge_add_admin_page(
			array(
				'slug' => '_title_page',
				'title' => esc_html__('Title', 'orkan'),
				'icon' => 'fa fa-list-alt'
			)
		);

		$panel_title = orkan_edge_add_admin_panel(
			array(
				'page' => '_title_page',
				'name' => 'panel_title',
				'title' => esc_html__('Title Settings', 'orkan')
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'show_title_area',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Title Area', 'orkan' ),
				'description'   => esc_html__( 'This option will enable/disable Title Area', 'orkan' ),
				'parent'        => $panel_title
			)
		);
		
			$show_title_area_container = orkan_edge_add_admin_container(
				array(
					'parent'          => $panel_title,
					'name'            => 'show_title_area_container',
					'dependency' => array(
						'show' => array(
							'show_title_area' => 'yes'
						)
					)
				)
			);
		
				orkan_edge_add_admin_field(
					array(
						'name'          => 'title_area_type',
						'type'          => 'select',
						'default_value' => $title_type_default_option,
						'label'         => esc_html__( 'Title Area Type', 'orkan' ),
						'description'   => esc_html__( 'Choose title type', 'orkan' ),
						'parent'        => $show_title_area_container,
						'options'       => $title_type_options
					)
				);
		
					orkan_edge_add_admin_field(
						array(
							'name'          => 'title_area_in_grid',
							'type'          => 'yesno',
							'default_value' => 'yes',
							'label'         => esc_html__( 'Title Area In Grid', 'orkan' ),
							'description'   => esc_html__( 'Set title area content to be in grid', 'orkan' ),
							'parent'        => $show_title_area_container
						)
					);
		
					orkan_edge_add_admin_field(
						array(
							'name'        => 'title_area_height',
							'type'        => 'text',
							'label'       => esc_html__( 'Height', 'orkan' ),
							'description' => esc_html__( 'Set a height for Title Area', 'orkan' ),
							'parent'      => $show_title_area_container,
							'args'        => array(
								'col_width' => 2,
								'suffix'    => 'px'
							)
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'name'        => 'title_area_background_color',
							'type'        => 'color',
							'label'       => esc_html__( 'Background Color', 'orkan' ),
							'description' => esc_html__( 'Choose a background color for Title Area', 'orkan' ),
							'parent'      => $show_title_area_container
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'name'        => 'title_area_background_image',
							'type'        => 'image',
							'label'       => esc_html__( 'Background Image', 'orkan' ),
							'description' => esc_html__( 'Choose an Image for Title Area', 'orkan' ),
							'parent'      => $show_title_area_container
						)
					);
		
					orkan_edge_add_admin_field(
						array(
							'name'          => 'title_area_background_image_behavior',
							'type'          => 'select',
							'default_value' => '',
							'label'         => esc_html__( 'Background Image Behavior', 'orkan' ),
							'description'   => esc_html__( 'Choose title area background image behavior', 'orkan' ),
							'parent'        => $show_title_area_container,
							'options'       => array(
								''                  => esc_html__( 'Default', 'orkan' ),
								'responsive'        => esc_html__( 'Enable Responsive Image', 'orkan' ),
								'parallax'          => esc_html__( 'Enable Parallax Image', 'orkan' ),
								'parallax-zoom-out' => esc_html__( 'Enable Parallax With Zoom Out Image', 'orkan' )
							)
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'name'          => 'title_area_vertical_alignment',
							'type'          => 'select',
							'default_value' => 'header-bottom',
							'label'         => esc_html__( 'Vertical Alignment', 'orkan' ),
							'description'   => esc_html__( 'Specify title vertical alignment', 'orkan' ),
							'parent'        => $show_title_area_container,
							'options'       => array(
								'header-bottom' => esc_html__( 'From Bottom of Header', 'orkan' ),
								'window-top'    => esc_html__( 'From Window Top', 'orkan' )
							)
						)
					);
		
		/***************** Additional Title Area Layout - start *****************/
		
		do_action( 'orkan_edge_additional_title_area_options_map', $show_title_area_container );
		
		/***************** Additional Title Area Layout - end *****************/
		
		
		$panel_typography = orkan_edge_add_admin_panel(
			array(
				'page'  => '_title_page',
				'name'  => 'panel_title_typography',
				'title' => esc_html__( 'Typography', 'orkan' )
			)
		);
		
			orkan_edge_add_admin_section_title(
				array(
					'name'   => 'type_section_title',
					'title'  => esc_html__( 'Title', 'orkan' ),
					'parent' => $panel_typography
				)
			);
		
			$group_page_title_styles = orkan_edge_add_admin_group(
				array(
					'name'        => 'group_page_title_styles',
					'title'       => esc_html__( 'Title', 'orkan' ),
					'description' => esc_html__( 'Define styles for page title', 'orkan' ),
					'parent'      => $panel_typography
				)
			);
		
				$row_page_title_styles_1 = orkan_edge_add_admin_row(
					array(
						'name'   => 'row_page_title_styles_1',
						'parent' => $group_page_title_styles
					)
				);
		
					orkan_edge_add_admin_field(
						array(
							'name'          => 'title_area_title_tag',
							'type'          => 'selectsimple',
							'default_value' => 'h1',
							'label'         => esc_html__( 'Title Tag', 'orkan' ),
							'options'       => orkan_edge_get_title_tag( false, array( 'span' => esc_html__( 'Custom Heading', 'orkan' ) ) ),
							'parent'        => $row_page_title_styles_1
						)
					);
		
				$row_page_title_styles_2 = orkan_edge_add_admin_row(
					array(
						'name'   => 'row_page_title_styles_2',
						'parent' => $group_page_title_styles
					)
				);
		
					orkan_edge_add_admin_field(
						array(
							'type'   => 'colorsimple',
							'name'   => 'page_title_color',
							'label'  => esc_html__( 'Text Color', 'orkan' ),
							'parent' => $row_page_title_styles_2
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'textsimple',
							'name'          => 'page_title_font_size',
							'default_value' => '',
							'label'         => esc_html__( 'Font Size', 'orkan' ),
							'parent'        => $row_page_title_styles_2,
							'args'          => array(
								'suffix' => 'px'
							)
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'textsimple',
							'name'          => 'page_title_line_height',
							'default_value' => '',
							'label'         => esc_html__( 'Line Height', 'orkan' ),
							'parent'        => $row_page_title_styles_2,
							'args'          => array(
								'suffix' => 'px'
							)
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'selectblanksimple',
							'name'          => 'page_title_text_transform',
							'default_value' => '',
							'label'         => esc_html__( 'Text Transform', 'orkan' ),
							'options'       => orkan_edge_get_text_transform_array(),
							'parent'        => $row_page_title_styles_2
						)
					);
		
				$row_page_title_styles_3 = orkan_edge_add_admin_row(
					array(
						'name'   => 'row_page_title_styles_3',
						'parent' => $group_page_title_styles,
						'next'   => true
					)
				);
		
					orkan_edge_add_admin_field(
						array(
							'type'          => 'fontsimple',
							'name'          => 'page_title_google_fonts',
							'default_value' => '-1',
							'label'         => esc_html__( 'Font Family', 'orkan' ),
							'parent'        => $row_page_title_styles_3
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'selectblanksimple',
							'name'          => 'page_title_font_style',
							'default_value' => '',
							'label'         => esc_html__( 'Font Style', 'orkan' ),
							'options'       => orkan_edge_get_font_style_array(),
							'parent'        => $row_page_title_styles_3
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'selectblanksimple',
							'name'          => 'page_title_font_weight',
							'default_value' => '',
							'label'         => esc_html__( 'Font Weight', 'orkan' ),
							'options'       => orkan_edge_get_font_weight_array(),
							'parent'        => $row_page_title_styles_3
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'textsimple',
							'name'          => 'page_title_letter_spacing',
							'default_value' => '',
							'label'         => esc_html__( 'Letter Spacing', 'orkan' ),
							'parent'        => $row_page_title_styles_3,
							'args'          => array(
								'suffix' => 'px'
							)
						)
					);
		
			orkan_edge_add_admin_section_title(
				array(
					'name'   => 'type_section_subtitle',
					'title'  => esc_html__( 'Subtitle', 'orkan' ),
					'parent' => $panel_typography
				)
			);
		
			$group_page_subtitle_styles = orkan_edge_add_admin_group(
				array(
					'name'        => 'group_page_subtitle_styles',
					'title'       => esc_html__( 'Subtitle', 'orkan' ),
					'description' => esc_html__( 'Define styles for page subtitle', 'orkan' ),
					'parent'      => $panel_typography
				)
			);
		
				$row_page_subtitle_styles_1 = orkan_edge_add_admin_row(
					array(
						'name'   => 'row_page_subtitle_styles_1',
						'parent' => $group_page_subtitle_styles
					)
				);
				
					orkan_edge_add_admin_field(
						array(
							'name' => 'title_area_subtitle_tag',
							'type' => 'selectsimple',
							'default_value' => 'p',
							'label' => esc_html__('Subtitle Tag', 'orkan'),
							'options' => orkan_edge_get_title_tag( false, array( 'p' => 'p' ) ),
							'parent' => $row_page_subtitle_styles_1
						)
					);
		
				$row_page_subtitle_styles_2 = orkan_edge_add_admin_row(
					array(
						'name'   => 'row_page_subtitle_styles_2',
						'parent' => $group_page_subtitle_styles
					)
				);
		
					orkan_edge_add_admin_field(
						array(
							'type'   => 'colorsimple',
							'name'   => 'page_subtitle_color',
							'label'  => esc_html__( 'Text Color', 'orkan' ),
							'parent' => $row_page_subtitle_styles_2
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'textsimple',
							'name'          => 'page_subtitle_font_size',
							'default_value' => '',
							'label'         => esc_html__( 'Font Size', 'orkan' ),
							'parent'        => $row_page_subtitle_styles_2,
							'args'          => array(
								'suffix' => 'px'
							)
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'textsimple',
							'name'          => 'page_subtitle_line_height',
							'default_value' => '',
							'label'         => esc_html__( 'Line Height', 'orkan' ),
							'parent'        => $row_page_subtitle_styles_2,
							'args'          => array(
								'suffix' => 'px'
							)
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'selectblanksimple',
							'name'          => 'page_subtitle_text_transform',
							'default_value' => '',
							'label'         => esc_html__( 'Text Transform', 'orkan' ),
							'options'       => orkan_edge_get_text_transform_array(),
							'parent'        => $row_page_subtitle_styles_2
						)
					);
		
				$row_page_subtitle_styles_3 = orkan_edge_add_admin_row(
					array(
						'name'   => 'row_page_subtitle_styles_3',
						'parent' => $group_page_subtitle_styles,
						'next'   => true
					)
				);
		
					orkan_edge_add_admin_field(
						array(
							'type'          => 'fontsimple',
							'name'          => 'page_subtitle_google_fonts',
							'default_value' => '-1',
							'label'         => esc_html__( 'Font Family', 'orkan' ),
							'parent'        => $row_page_subtitle_styles_3
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'selectblanksimple',
							'name'          => 'page_subtitle_font_style',
							'default_value' => '',
							'label'         => esc_html__( 'Font Style', 'orkan' ),
							'options'       => orkan_edge_get_font_style_array(),
							'parent'        => $row_page_subtitle_styles_3
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'selectblanksimple',
							'name'          => 'page_subtitle_font_weight',
							'default_value' => '',
							'label'         => esc_html__( 'Font Weight', 'orkan' ),
							'options'       => orkan_edge_get_font_weight_array(),
							'parent'        => $row_page_subtitle_styles_3
						)
					);
					
					orkan_edge_add_admin_field(
						array(
							'type'          => 'textsimple',
							'name'          => 'page_subtitle_letter_spacing',
							'default_value' => '',
							'label'         => esc_html__( 'Letter Spacing', 'orkan' ),
							'args'          => array(
								'suffix' => 'px'
							),
							'parent'        => $row_page_subtitle_styles_3
						)
					);
		
		/***************** Additional Title Typography Layout - start *****************/
		
		do_action( 'orkan_edge_additional_title_typography_options_map', $panel_typography );
		
		/***************** Additional Title Typography Layout - end *****************/
    }

	add_action( 'orkan_edge_options_map', 'orkan_edge_title_options_map', 6);
}