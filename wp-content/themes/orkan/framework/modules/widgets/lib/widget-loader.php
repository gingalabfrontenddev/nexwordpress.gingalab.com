<?php

if ( ! function_exists( 'orkan_edge_register_widgets' ) ) {
	function orkan_edge_register_widgets() {
		$widgets = apply_filters( 'orkan_edge_register_widgets', $widgets = array() );
		
		foreach ( $widgets as $widget ) {
			register_widget( $widget );
		}
	}
	
	add_action( 'widgets_init', 'orkan_edge_register_widgets' );
}