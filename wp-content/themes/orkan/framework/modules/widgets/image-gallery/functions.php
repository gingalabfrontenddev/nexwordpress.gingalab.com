<?php

if ( ! function_exists( 'orkan_edge_register_image_gallery_widget' ) ) {
	/**
	 * Function that register image gallery widget
	 */
	function orkan_edge_register_image_gallery_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassImageGalleryWidget';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_image_gallery_widget' );
}