<?php

if ( ! function_exists( 'orkan_edge_register_blog_list_widget' ) ) {
	/**
	 * Function that register blog list widget
	 */
	function orkan_edge_register_blog_list_widget( $widgets ) {
		$widgets[] = 'OrkanEdgeClassBlogListWidget';
		
		return $widgets;
	}
	
	add_filter( 'orkan_edge_register_widgets', 'orkan_edge_register_blog_list_widget' );
}