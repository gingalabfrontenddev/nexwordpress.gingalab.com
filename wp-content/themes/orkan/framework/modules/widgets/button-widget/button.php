<?php

class OrkanEdgeClassButtonWidget extends OrkanEdgeClassWidget {
	public function __construct() {
		parent::__construct(
			'edgtf_button_widget',
			esc_html__( 'Edge Button Widget', 'orkan' ),
			array( 'description' => esc_html__( 'Add button element to widget areas', 'orkan' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'    => 'dropdown',
				'name'    => 'type',
				'title'   => esc_html__( 'Type', 'orkan' ),
				'options' => array(
					'solid'   => esc_html__( 'Solid', 'orkan' ),
					'outline' => esc_html__( 'Outline', 'orkan' ),
					'simple'  => esc_html__( 'Simple', 'orkan' )
				)
			),
			array(
				'type'        => 'dropdown',
				'name'        => 'size',
				'title'       => esc_html__( 'Size', 'orkan' ),
				'options'     => array(
					'small'  => esc_html__( 'Small', 'orkan' ),
					'medium' => esc_html__( 'Medium', 'orkan' ),
					'large'  => esc_html__( 'Large', 'orkan' ),
					'huge'   => esc_html__( 'Huge', 'orkan' )
				),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'orkan' )
			),
			array(
				'type'    => 'textfield',
				'name'    => 'text',
				'title'   => esc_html__( 'Text', 'orkan' ),
				'default' => esc_html__( 'Button Text', 'orkan' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'link',
				'title' => esc_html__( 'Link', 'orkan' )
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'target',
				'title'   => esc_html__( 'Link Target', 'orkan' ),
				'options' => orkan_edge_get_link_target_array()
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'color',
				'title' => esc_html__( 'Color', 'orkan' )
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'hover_color',
				'title' => esc_html__( 'Hover Color', 'orkan' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'background_color',
				'title'       => esc_html__( 'Background Color', 'orkan' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'orkan' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'hover_background_color',
				'title'       => esc_html__( 'Hover Background Color', 'orkan' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'orkan' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'border_color',
				'title'       => esc_html__( 'Border Color', 'orkan' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'orkan' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'hover_border_color',
				'title'       => esc_html__( 'Hover Border Color', 'orkan' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'orkan' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'margin',
				'title'       => esc_html__( 'Margin', 'orkan' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'orkan' )
			)
		);
	}
	
	public function widget( $args, $instance ) {
		$params = '';
		
		if ( ! is_array( $instance ) ) {
			$instance = array();
		}
		
		// Filter out all empty params
		$instance = array_filter( $instance, function ( $array_value ) {
			return trim( $array_value ) != '';
		} );
		
		// Default values
		if ( ! isset( $instance['text'] ) ) {
			$instance['text'] = 'Button Text';
		}
		
		// Generate shortcode params
		foreach ( $instance as $key => $value ) {
			$params .= " $key='$value' ";
		}
		
		echo '<div class="widget edgtf-button-widget">';
			echo do_shortcode( "[edgtf_button $params]" ); // XSS OK
		echo '</div>';
	}
}