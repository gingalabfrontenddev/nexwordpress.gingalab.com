<?php

if ( ! function_exists( 'orkan_edge_map_woocommerce_meta' ) ) {
	function orkan_edge_map_woocommerce_meta() {
		
		$woocommerce_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => array( 'product' ),
				'title' => esc_html__( 'Product Meta', 'orkan' ),
				'name'  => 'woo_product_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_product_featured_image_size',
				'type'        => 'select',
				'label'       => esc_html__( 'Dimensions for Product List Shortcode', 'orkan' ),
				'description' => esc_html__( 'Choose image layout when it appears in Edge Product List - Masonry layout shortcode', 'orkan' ),
				'options'     => array(
					''                                   => esc_html__( 'Default', 'orkan' ),
					'edgtf-woo-image-small'              => esc_html__( 'Small', 'orkan' ),
					'edgtf-woo-image-large-width'        => esc_html__( 'Large Width', 'orkan' ),
					'edgtf-woo-image-large-height'       => esc_html__( 'Large Height', 'orkan' ),
					'edgtf-woo-image-large-width-height' => esc_html__( 'Large Width Height', 'orkan' )
				),
				'parent'      => $woocommerce_meta_box
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_show_title_area_woo_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'orkan' ),
				'description'   => esc_html__( 'Disabling this option will turn off page title area', 'orkan' ),
				'options'       => orkan_edge_get_yes_no_select_array(),
				'parent'        => $woocommerce_meta_box
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_show_new_sign_woo_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Show New Sign', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show new sign mark on product', 'orkan' ),
				'parent'        => $woocommerce_meta_box
			)
		);
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_woocommerce_meta', 99 );
}