<?php

if ( ! function_exists( 'orkan_edge_search_body_class' ) ) {
	/**
	 * Function that adds body classes for different search types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function orkan_edge_search_body_class( $classes ) {
		$classes[] = 'edgtf-search-covers-header';
		
		return $classes;
	}
	
	add_filter( 'body_class', 'orkan_edge_search_body_class' );
}

if ( ! function_exists( 'orkan_edge_get_search' ) ) {
	/**
	 * Loads search HTML based on search type option.
	 */
	function orkan_edge_get_search() {
		orkan_edge_load_search_template();
	}
	
	add_action( 'orkan_edge_before_page_header_html_close', 'orkan_edge_get_search' );
	add_action( 'orkan_edge_before_mobile_header_html_close', 'orkan_edge_get_search' );
}

if ( ! function_exists( 'orkan_edge_load_search_template' ) ) {
	/**
	 * Loads search HTML based on search type option.
	 */
	function orkan_edge_load_search_template() {
		$search_icon       = '';
		$search_icon_close = '';
		
		$search_in_grid   = orkan_edge_options()->getOptionValue( 'search_in_grid' ) == 'yes' ? true : false;
		$search_icon_pack = orkan_edge_options()->getOptionValue( 'search_icon_pack' );
		
		if ( ! empty( $search_icon_pack ) ) {
			$search_icon       = orkan_edge_icon_collections()->getSearchIcon( $search_icon_pack, true );
			$search_icon_close = orkan_edge_icon_collections()->getSearchClose( 'font_elegant', true );
		}
		
		$parameters = array(
			'search_in_grid'    => $search_in_grid,
			'search_icon'       => $search_icon,
			'search_icon_close' => $search_icon_close
		);
		
		orkan_edge_get_module_template_part( 'types/covers-header/templates/covers-header', 'search', '', $parameters );
	}
}