<?php

if ( ! function_exists( 'orkan_edge_register_sidebars' ) ) {
	/**
	 * Function that registers theme's sidebars
	 */
	function orkan_edge_register_sidebars() {
		
		register_sidebar(
			array(
				'id'            => 'sidebar',
				'name'          => esc_html__( 'Sidebar', 'orkan' ),
				'description'   => esc_html__( 'Default Sidebar', 'orkan' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="edgtf-widget-title">',
				'after_title'   => '</h4>'
			)
		);
	}
	
	add_action( 'widgets_init', 'orkan_edge_register_sidebars', 1 );
}

if ( ! function_exists( 'orkan_edge_add_support_custom_sidebar' ) ) {
	/**
	 * Function that adds theme support for custom sidebars. It also creates OrkanEdgeClassSidebar object
	 */
	function orkan_edge_add_support_custom_sidebar() {
		add_theme_support( 'OrkanEdgeClassSidebar' );
		
		if ( get_theme_support( 'OrkanEdgeClassSidebar' ) ) {
			new OrkanEdgeClassSidebar();
		}
	}
	
	add_action( 'after_setup_theme', 'orkan_edge_add_support_custom_sidebar' );
}