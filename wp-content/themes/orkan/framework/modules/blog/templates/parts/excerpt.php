<?php

if ( post_password_required() ) {
	echo get_the_password_form();
} else {
	$excerpt_length = isset( $params['excerpt_length'] ) ? $params['excerpt_length'] : '';
	$excerpt        = orkan_edge_excerpt( $excerpt_length );
	
	$link_page_exists = apply_filters( 'orkan_edge_filter_single_link_pages', '' );
	
	if ( ! empty( $excerpt ) && empty( $link_page_exists ) ) { ?>
		<div class="edgtf-post-excerpt-holder">
			<p itemprop="description" class="edgtf-post-excerpt">
				<?php echo wp_kses_post( $excerpt ); ?> [...]
			</p>
		</div>
	<?php }
} ?>
