<?php
$share_type = isset($share_type) ? $share_type : 'list';
?>
<?php if(orkan_edge_options()->getOptionValue('enable_social_share') === 'yes' && orkan_edge_options()->getOptionValue('enable_social_share_on_post') === 'yes') { ?>
    <div class="edgtf-blog-share">
        <?php echo orkan_edge_get_social_share_html(array('type' => $share_type)); ?>
    </div>
<?php } ?>