<?php

if ( ! function_exists( 'orkan_edge_map_post_audio_meta' ) ) {
	function orkan_edge_map_post_audio_meta() {
		$audio_post_format_meta_box = orkan_edge_add_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Audio Post Format', 'orkan' ),
				'name'  => 'post_format_audio_meta'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_audio_type_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Audio Type', 'orkan' ),
				'description'   => esc_html__( 'Choose audio type', 'orkan' ),
				'parent'        => $audio_post_format_meta_box,
				'default_value' => 'social_networks',
				'options'       => array(
					'social_networks' => esc_html__( 'Audio Service', 'orkan' ),
					'self'            => esc_html__( 'Self Hosted', 'orkan' )
				)
			)
		);
		
		$edgtf_audio_embedded_container = orkan_edge_add_admin_container(
			array(
				'parent' => $audio_post_format_meta_box,
				'name'   => 'edgtf_audio_embedded_container'
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_audio_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Audio URL', 'orkan' ),
				'description' => esc_html__( 'Enter audio URL', 'orkan' ),
				'parent'      => $edgtf_audio_embedded_container,
				'dependency' => array(
					'show' => array(
						'edgtf_audio_type_meta' => 'social_networks'
					)
				)
			)
		);
		
		orkan_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_audio_custom_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Audio Link', 'orkan' ),
				'description' => esc_html__( 'Enter audio link', 'orkan' ),
				'parent'      => $edgtf_audio_embedded_container,
				'dependency' => array(
					'show' => array(
						'edgtf_audio_type_meta' => 'self'
					)
				)
			)
		);
	}
	
	add_action( 'orkan_edge_meta_boxes_map', 'orkan_edge_map_post_audio_meta', 23 );
}