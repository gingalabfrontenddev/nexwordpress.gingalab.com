<?php

if ( ! function_exists( 'orkan_edge_get_blog_list_types_options' ) ) {
	function orkan_edge_get_blog_list_types_options() {
		$blog_list_type_options = apply_filters( 'orkan_edge_blog_list_type_global_option', $blog_list_type_options = array() );
		
		return $blog_list_type_options;
	}
}

if ( ! function_exists( 'orkan_edge_blog_options_map' ) ) {
	function orkan_edge_blog_options_map() {
		$blog_list_type_options = orkan_edge_get_blog_list_types_options();
		
		orkan_edge_add_admin_page(
			array(
				'slug'  => '_blog_page',
				'title' => esc_html__( 'Blog', 'orkan' ),
				'icon'  => 'fa fa-files-o'
			)
		);
		
		/**
		 * Blog Lists
		 */
		$panel_blog_lists = orkan_edge_add_admin_panel(
			array(
				'page'  => '_blog_page',
				'name'  => 'panel_blog_lists',
				'title' => esc_html__( 'Blog Lists', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_list_type',
				'type'          => 'select',
				'label'         => esc_html__( 'Blog Layout for Archive Pages', 'orkan' ),
				'description'   => esc_html__( 'Choose a default blog layout for archived blog post lists', 'orkan' ),
				'default_value' => 'standard',
				'parent'        => $panel_blog_lists,
				'options'       => $blog_list_type_options
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'archive_sidebar_layout',
				'type'          => 'select',
				'label'         => esc_html__( 'Sidebar Layout for Archive Pages', 'orkan' ),
				'description'   => esc_html__( 'Choose a sidebar layout for archived blog post lists', 'orkan' ),
				'default_value' => '',
				'parent'        => $panel_blog_lists,
                'options'       => orkan_edge_get_custom_sidebars_options(),
			)
		);
		
		$orkan_custom_sidebars = orkan_edge_get_custom_sidebars();
		if ( count( $orkan_custom_sidebars ) > 0 ) {
			orkan_edge_add_admin_field(
				array(
					'name'        => 'archive_custom_sidebar_area',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Sidebar to Display for Archive Pages', 'orkan' ),
					'description' => esc_html__( 'Choose a sidebar to display on archived blog post lists. Default sidebar is "Sidebar Page"', 'orkan' ),
					'parent'      => $panel_blog_lists,
					'options'     => orkan_edge_get_custom_sidebars(),
					'args'        => array(
						'select2' => true
					)
				)
			);
		}
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_masonry_layout',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Layout', 'orkan' ),
				'default_value' => 'in-grid',
				'description'   => esc_html__( 'Set masonry layout. Default is in grid.', 'orkan' ),
				'parent'        => $panel_blog_lists,
				'options'       => array(
					'in-grid'    => esc_html__( 'In Grid', 'orkan' ),
					'full-width' => esc_html__( 'Full Width', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_masonry_number_of_columns',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Number of Columns', 'orkan' ),
				'default_value' => 'three',
				'description'   => esc_html__( 'Set number of columns for your masonry blog lists. Default value is 4 columns', 'orkan' ),
				'parent'        => $panel_blog_lists,
				'options'       => array(
					'two'   => esc_html__( '2 Columns', 'orkan' ),
					'three' => esc_html__( '3 Columns', 'orkan' ),
					'four'  => esc_html__( '4 Columns', 'orkan' ),
					'five'  => esc_html__( '5 Columns', 'orkan' ),
					'six'   => esc_html__( '6 Columns', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_masonry_space_between_items',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Space Between Items', 'orkan' ),
				'description'   => esc_html__( 'Set space size between posts for your masonry blog lists. Default value is normal', 'orkan' ),
				'default_value' => 'normal',
				'options'       => orkan_edge_get_space_between_items_array(),
				'parent'        => $panel_blog_lists
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_list_featured_image_proportion',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Featured Image Proportion', 'orkan' ),
				'default_value' => 'fixed',
				'description'   => esc_html__( 'Choose type of proportions you want to use for featured images on masonry blog lists', 'orkan' ),
				'parent'        => $panel_blog_lists,
				'options'       => array(
					'fixed'    => esc_html__( 'Fixed', 'orkan' ),
					'original' => esc_html__( 'Original', 'orkan' )
				)
			)
		);

		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_list_article_appear_fx',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Article Appear Effect', 'orkan' ),
				'default_value' => 'no',
				'description'   => esc_html__( 'Choose whether to have article appear animation when list is loaded.', 'orkan' ),
				'parent'        => $panel_blog_lists,
				'options'       => array(
					'no'    => esc_html__( 'No', 'orkan' ),
					'yes' 	=> esc_html__( 'Yes', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_pagination_type',
				'type'          => 'select',
				'label'         => esc_html__( 'Pagination Type', 'orkan' ),
				'description'   => esc_html__( 'Choose a pagination layout for Blog Lists', 'orkan' ),
				'parent'        => $panel_blog_lists,
				'default_value' => 'standard',
				'options'       => array(
					'standard'        => esc_html__( 'Standard', 'orkan' ),
					'load-more'       => esc_html__( 'Load More', 'orkan' ),
					'infinite-scroll' => esc_html__( 'Infinite Scroll', 'orkan' ),
					'no-pagination'   => esc_html__( 'No Pagination', 'orkan' )
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'type'          => 'text',
				'name'          => 'number_of_chars',
				'default_value' => '40',
				'label'         => esc_html__( 'Number of Words in Excerpt', 'orkan' ),
				'description'   => esc_html__( 'Enter a number of words in excerpt (article summary). Default value is 40', 'orkan' ),
				'parent'        => $panel_blog_lists,
				'args'          => array(
					'col_width' => 3
				)
			)
		);
		
		/**
		 * Blog Single
		 */
		$panel_blog_single = orkan_edge_add_admin_panel(
			array(
				'page'  => '_blog_page',
				'name'  => 'panel_blog_single',
				'title' => esc_html__( 'Blog Single', 'orkan' )
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_single_sidebar_layout',
				'type'          => 'select',
				'label'         => esc_html__( 'Sidebar Layout', 'orkan' ),
				'description'   => esc_html__( 'Choose a sidebar layout for Blog Single pages', 'orkan' ),
				'default_value' => '',
				'parent'        => $panel_blog_single,
                'options'       => orkan_edge_get_custom_sidebars_options()
			)
		);
		
		if ( count( $orkan_custom_sidebars ) > 0 ) {
			orkan_edge_add_admin_field(
				array(
					'name'        => 'blog_single_custom_sidebar_area',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Sidebar to Display', 'orkan' ),
					'description' => esc_html__( 'Choose a sidebar to display on Blog Single pages. Default sidebar is "Sidebar"', 'orkan' ),
					'parent'      => $panel_blog_single,
					'options'     => orkan_edge_get_custom_sidebars(),
					'args'        => array(
						'select2' => true
					)
				)
			);
		}
		
		orkan_edge_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'show_title_area_blog',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show title area on single post pages', 'orkan' ),
				'parent'        => $panel_blog_single,
				'options'       => orkan_edge_get_yes_no_select_array(),
				'args'          => array(
					'col_width' => 3
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_single_title_in_title_area',
				'type'          => 'yesno',
				'label'         => esc_html__( 'Show Post Title in Title Area', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show post title in title area on single post pages', 'orkan' ),
				'parent'        => $panel_blog_single,
				'default_value' => 'no'
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_single_related_posts',
				'type'          => 'yesno',
				'label'         => esc_html__( 'Show Related Posts', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show related posts on single post pages', 'orkan' ),
				'parent'        => $panel_blog_single,
				'default_value' => 'yes'
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'name'          => 'blog_single_comments',
				'type'          => 'yesno',
				'label'         => esc_html__( 'Show Comments Form', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show comments form on single post pages', 'orkan' ),
				'parent'        => $panel_blog_single,
				'default_value' => 'yes'
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'blog_single_navigation',
				'default_value' => 'no',
				'label'         => esc_html__( 'Enable Prev/Next Single Post Navigation Links', 'orkan' ),
				'description'   => esc_html__( 'Enable navigation links through the blog posts (left and right arrows will appear)', 'orkan' ),
				'parent'        => $panel_blog_single
			)
		);
		
		$blog_single_navigation_container = orkan_edge_add_admin_container(
			array(
				'name'            => 'edgtf_blog_single_navigation_container',
				'parent'          => $panel_blog_single,
				'dependency' => array(
					'show' => array(
						'blog_single_navigation' => 'yes'
					)
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'blog_navigation_through_same_category',
				'default_value' => 'no',
				'label'         => esc_html__( 'Enable Navigation Only in Current Category', 'orkan' ),
				'description'   => esc_html__( 'Limit your navigation only through current category', 'orkan' ),
				'parent'        => $blog_single_navigation_container,
				'args'          => array(
					'col_width' => 3
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'blog_author_info',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Author Info Box', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will display author name and descriptions on single post pages', 'orkan' ),
				'parent'        => $panel_blog_single
			)
		);
		
		$blog_single_author_info_container = orkan_edge_add_admin_container(
			array(
				'name'            => 'edgtf_blog_single_author_info_container',
				'parent'          => $panel_blog_single,
				'dependency' => array(
					'show' => array(
						'blog_author_info' => 'yes'
					)
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'blog_author_info_email',
				'default_value' => 'no',
				'label'         => esc_html__( 'Show Author Email', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show author email', 'orkan' ),
				'parent'        => $blog_single_author_info_container,
				'args'          => array(
					'col_width' => 3
				)
			)
		);
		
		orkan_edge_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'blog_single_author_social',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Author Social Icons', 'orkan' ),
				'description'   => esc_html__( 'Enabling this option will show author social icons on single post pages', 'orkan' ),
				'parent'        => $blog_single_author_info_container,
				'args'          => array(
					'col_width' => 3
				)
			)
		);
		
		do_action( 'orkan_edge_blog_single_options_map', $panel_blog_single );
	}
	
	add_action( 'orkan_edge_options_map', 'orkan_edge_blog_options_map', 13 );
}