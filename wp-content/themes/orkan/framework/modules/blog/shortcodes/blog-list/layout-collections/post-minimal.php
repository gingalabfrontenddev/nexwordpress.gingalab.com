<li class="edgtf-bl-item edgtf-item-space clearfix">
	<div class="edgtf-bli-inner">
		<div class="edgtf-bli-content">
			<div class="edgtf-bli-info edgtf-bli-info-top">
				<?php orkan_edge_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params ); ?>
			</div>
			<?php orkan_edge_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
		</div>
	</div>
</li>