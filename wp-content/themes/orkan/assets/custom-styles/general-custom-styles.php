<?php

if(!function_exists('orkan_edge_design_styles')) {
    /**
     * Generates general custom styles
     */
    function orkan_edge_design_styles() {
	    $font_family = orkan_edge_options()->getOptionValue( 'google_fonts' );
	    if ( ! empty( $font_family ) && orkan_edge_is_font_option_valid( $font_family ) ) {
		    $font_family_selector = array(
			    'body'
		    );
		    echo orkan_edge_dynamic_css( $font_family_selector, array( 'font-family' => orkan_edge_get_font_option_val( $font_family ) ) );
	    }

		$first_main_color = orkan_edge_options()->getOptionValue('first_color');
        if(!empty($first_main_color)) {
            $color_selector = array(
	            'a:hover',
	            'h1 a:hover',
	            'h2 a:hover',
	            'h3 a:hover',
	            'h4 a:hover',
	            'h5 a:hover',
	            'h6 a:hover',
	            'p a:hover',
	            '.edgtf-comment-holder .edgtf-comment-text #cancel-comment-reply-link',
	            '.edgtf-owl-slider .owl-nav .owl-next:hover',
	            '.edgtf-owl-slider .owl-nav .owl-prev:hover',
	            '.widget.widget_rss .edgtf-widget-title .rsswidget:hover',
	            '.widget.widget_search button:hover',
	            '.edgtf-footer-top-holder .widget a:hover',
	            '.edgtf-side-menu .widget a:hover',
	            '.edgtf-vertical-menu-area .widget a:hover',
	            '.edgtf-footer-top-holder .widget.widget_rss .edgtf-footer-widget-title .rsswidget:hover',
	            '.edgtf-side-menu .widget.widget_rss .edgtf-footer-widget-title .rsswidget:hover',
	            '.edgtf-vertical-menu-area .widget.widget_rss .edgtf-footer-widget-title .rsswidget:hover',
	            '.edgtf-footer-top-holder .widget.widget_search button:hover',
	            '.edgtf-side-menu .widget.widget_search button:hover',
	            '.edgtf-vertical-menu-area .widget.widget_search button:hover',
	            '.edgtf-footer-top-holder .widget.widget_tag_cloud a:hover',
	            '.edgtf-side-menu .widget.widget_tag_cloud a:hover',
	            '.edgtf-vertical-menu-area .widget.widget_tag_cloud a:hover',
	            '.edgtf-top-bar a:hover',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-standard li .edgtf-twitter-icon',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-tweet-text a',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-tweet-text span',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-standard li .edgtf-tweet-text a:hover',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-twitter-icon i',
	            '.widget_icl_lang_sel_widget .wpml-ls-legacy-dropdown .wpml-ls-item-toggle:hover',
	            '.widget_icl_lang_sel_widget .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle:hover',
	            '.edgtf-blog-holder article.sticky .edgtf-post-title a',
	            '.edgtf-blog-holder article .edgtf-post-info-standard>div a:hover',
	            '.edgtf-blog-holder article .edgtf-post-info-standard-bottom>div a:not(.edgtf-share-link):hover',
	            '.edgtf-blog-holder article .edgtf-post-info-standard-bottom>div.edgtf-blog-like a.liked i',
	            '.edgtf-blog-holder article.format-link .edgtf-post-title a:hover',
	            '.edgtf-bl-standard-pagination ul li.edgtf-bl-pag-active a',
	            '.edgtf-blog-pagination ul li a.edgtf-pag-active',
	            '.edgtf-blog-single-navigation .edgtf-blog-single-next',
	            '.edgtf-blog-single-navigation .edgtf-blog-single-prev',
	            '.edgtf-blog-single-navigation .edgtf-blog-single-next:hover .edgtf-blog-single-nav-title',
	            '.edgtf-blog-single-navigation .edgtf-blog-single-prev:hover .edgtf-blog-single-nav-title',
	            '.edgtf-blog-slider-holder.edgtf-bs-slider .edgtf-bli-info>div a:hover',
	            '.edgtf-blog-slider-holder.edgtf-bs-slider .edgtf-post-title a:hover',
	            '.edgtf-blog-slider-holder.edgtf-bs-carousel-centered .edgtf-bli-info>div a:hover',
	            '.edgtf-blog-slider-holder.edgtf-bs-carousel-centered .edgtf-post-title a:hover',
	            '.edgtf-main-menu ul li:hover>a',
	            '.edgtf-main-menu>ul>li.edgtf-active-item>a',
	            '.edgtf-main-menu>ul>li>a:hover',
	            '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-main-menu>ul>li.edgtf-active-item>a',
	            '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-main-menu>ul>li>a:hover',
	            '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-main-menu>ul>li.edgtf-active-item>a',
	            '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-main-menu>ul>li>a:hover',
	            '.edgtf-drop-down .second .inner ul li.current-menu-ancestor>a',
	            '.edgtf-drop-down .second .inner ul li.current-menu-item>a',
	            '.edgtf-drop-down .wide .second .inner>ul>li.current-menu-ancestor>a',
	            '.edgtf-drop-down .wide .second .inner>ul>li.current-menu-item>a',
	            '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-fullscreen-menu-opener.edgtf-fm-opened',
	            '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-fullscreen-menu-opener:hover',
	            '.edgtf-fullscreen-menu-opener:hover',
	            '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-fullscreen-menu-opener.edgtf-fm-opened',
	            '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-fullscreen-menu-opener:hover',
	            '.edgtf-fullscreen-menu-opener.edgtf-fm-opened:hover:after',
	            'nav.edgtf-fullscreen-menu ul li ul li.current-menu-ancestor>a',
	            'nav.edgtf-fullscreen-menu ul li ul li.current-menu-item>a',
	            'nav.edgtf-fullscreen-menu>ul>li.edgtf-active-item>a',
	            '.edgtf-header-vertical .edgtf-vertical-menu ul li a:hover',
	            '.edgtf-header-vertical .edgtf-vertical-menu ul li.current-menu-ancestor>a',
	            '.edgtf-header-vertical .edgtf-vertical-menu ul li.current-menu-item>a',
	            '.edgtf-header-vertical .edgtf-vertical-menu ul li.current_page_item>a',
	            '.edgtf-header-vertical .edgtf-vertical-menu ul li.edgtf-active-item>a',
	            '.edgtf-header-vertical .edgtf-vertical-menu ul li a .edgtf-menu-featured-icon',
	            '.edgtf-header-vertical .edgtf-vertical-menu>ul>li>a:hover',
	            '.edgtf-mobile-header .edgtf-mobile-menu-opener a:hover',
	            '.edgtf-mobile-header .edgtf-mobile-menu-opener.edgtf-mobile-menu-opened a',
	            '.edgtf-mobile-header .edgtf-mobile-nav .edgtf-grid>ul>li.edgtf-active-item>a',
	            '.edgtf-mobile-header .edgtf-mobile-nav .edgtf-grid>ul>li.edgtf-active-item>h6',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul li a:hover',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul li h6:hover',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-ancestor>a',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-ancestor>h6',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-item>a',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-item>h6',
	            '.edgtf-main-menu.edgtf-sticky-nav>ul>li.edgtf-active-item>a',
	            '.edgtf-main-menu.edgtf-sticky-nav>ul>li>a:hover',
	            '.edgtf-search-page-holder article.sticky .edgtf-post-title a',
	            '.edgtf-search-cover .edgtf-search-close:hover',
	            '.edgtf-side-menu-button-opener.opened',
	            '.edgtf-side-menu-button-opener:hover',
	            '.edgtf-side-menu a.edgtf-close-side-menu:hover',
	            '.edgtf-portfolio-single-holder .edgtf-ps-info-holder .edgtf-ps-info-item a:not(.edgtf-share-link):hover',
	            '.edgtf-portfolio-full-screen-slider-holder #edgtf-ptf-info-block .edgtf-pli-info-holder .edgtf-pli-info p a:hover',
	            '.edgtf-portfolio-full-screen-slider-holder .edgtf-portfolio-list-holder.edgtf-nav-light-skin .edgtf-next:hover',
	            '.edgtf-portfolio-full-screen-slider-holder .edgtf-portfolio-list-holder.edgtf-nav-light-skin .edgtf-prev:hover',
	            '.edgtf-portfolio-full-screen-slider-holder .edgtf-portfolio-list-holder.edgtf-nav-dark-skin .edgtf-next:hover',
	            '.edgtf-portfolio-full-screen-slider-holder .edgtf-portfolio-list-holder.edgtf-nav-dark-skin .edgtf-prev:hover',
	            '.edgtf-pl-filter-holder ul li.edgtf-pl-current span',
	            '.edgtf-pl-filter-holder ul li:hover span',
	            '.edgtf-pl-standard-pagination ul li.edgtf-pl-pag-active a',
	            '.edgtf-portfolio-list-holder.edgtf-pl-gallery-overlay .edgtf-pli-text-holder .edgtf-pli-text .edgtf-pli-category-holder a:hover',
	            '.edgtf-portfolio-slider-holder .edgtf-portfolio-list-holder.edgtf-nav-light-skin .owl-nav .owl-next:hover',
	            '.edgtf-portfolio-slider-holder .edgtf-portfolio-list-holder.edgtf-nav-light-skin .owl-nav .owl-prev:hover',
	            '.edgtf-portfolio-slider-holder .edgtf-portfolio-list-holder.edgtf-nav-dark-skin .owl-nav .owl-next:hover',
	            '.edgtf-portfolio-slider-holder .edgtf-portfolio-list-holder.edgtf-nav-dark-skin .owl-nav .owl-prev:hover',
	            '.edgtf-banner-holder .edgtf-banner-link-text .edgtf-banner-link-hover span',
	            '.edgtf-btn.edgtf-btn-simple',
	            '.edgtf-btn.edgtf-btn-solid.edgtf-hover-animation.edgtf-btn-white-blue',
	            '.edgtf-btn.edgtf-btn-solid.edgtf-hover-animation.edgtf-btn-white-black',
	            '.edgtf-btn.edgtf-btn-outline',
	            '.edgtf-counter-holder .edgtf-counter',
	            '.edgtf-section-title-holder .edgtf-st-title-dot',
	            '.edgtf-social-share-holder.edgtf-dropdown .edgtf-social-share-dropdown-opener:hover',
	            '.edgtf-twitter-list-holder .edgtf-twitter-icon',
	            '.edgtf-twitter-list-holder .edgtf-tweet-text a:hover',
	            '.edgtf-twitter-list-holder .edgtf-twitter-profile a:hover'
            );

            $woo_color_selector = array();
            if(orkan_edge_is_woocommerce_installed()) {
                $woo_color_selector = array(
	                '.woocommerce-pagination .page-numbers li a.current',
	                '.woocommerce-pagination .page-numbers li a:hover',
	                '.woocommerce-pagination .page-numbers li span.current',
	                '.woocommerce-pagination .page-numbers li span:hover',
	                '.woocommerce-page .edgtf-content .edgtf-quantity-buttons .edgtf-quantity-minus:hover',
	                '.woocommerce-page .edgtf-content .edgtf-quantity-buttons .edgtf-quantity-plus:hover',
	                'div.woocommerce .edgtf-quantity-buttons .edgtf-quantity-minus:hover',
	                'div.woocommerce .edgtf-quantity-buttons .edgtf-quantity-plus:hover',
	                '.woocommerce .star-rating span',
	                'ul.products>.product .edgtf-pl-category a:hover',
	                '.edgtf-woo-single-page .edgtf-single-product-summary .woocommerce-product-rating .woocommerce-review-link:hover',
	                '.edgtf-woo-single-page .edgtf-single-product-summary .product_meta>span a:hover',
	                '.edgtf-woo-single-page .woocommerce-tabs #reviews .comment-respond .stars a.active:after',
	                '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-shopping-cart-holder .edgtf-header-cart:hover',
	                '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-shopping-cart-holder .edgtf-header-cart:hover',
	                '.edgtf-shopping-cart-dropdown .edgtf-item-info-holder .edgtf-product-title a:hover',
	                '.edgtf-shopping-cart-dropdown .edgtf-item-info-holder .remove:hover',
	                '.widget.woocommerce.widget_layered_nav ul li.chosen a',
	                '.widget.woocommerce.widget_product_categories ul li a:hover',
	                '.widget.woocommerce.widget_product_search .woocommerce-product-search button:hover',
	                '.edgtf-pl-holder .edgtf-pli .edgtf-pli-category a:hover',
	                '.edgtf-pl-holder .edgtf-pli .edgtf-pli-rating span'
                );
            }

            $color_selector = array_merge($color_selector, $woo_color_selector);

	        $color_important_selector = array(
		        '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-icon-widget-holder:hover',
		        '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-icon-widget-holder:hover',
		        '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-social-icon-widget-holder:hover',
		        '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-social-icon-widget-holder:hover',
		        '.edgtf-light-header .edgtf-page-header>div:not(.fixed):not(.edgtf-sticky-header) .edgtf-menu-area .widget a:hover',
		        '.edgtf-light-header .edgtf-page-header>div:not(.fixed):not(.edgtf-sticky-header).edgtf-menu-area .widget a:hover',
		        '.edgtf-dark-header .edgtf-page-header>div:not(.fixed):not(.edgtf-sticky-header) .edgtf-menu-area .widget a:hover',
		        '.edgtf-dark-header .edgtf-page-header>div:not(.fixed):not(.edgtf-sticky-header).edgtf-menu-area .widget a:hover',
		        '.edgtf-light-header.edgtf-header-vertical .edgtf-vertical-menu ul li a:hover',
		        '.edgtf-light-header.edgtf-header-vertical .edgtf-vertical-menu ul li ul li.current-menu-ancestor>a',
		        '.edgtf-light-header.edgtf-header-vertical .edgtf-vertical-menu ul li ul li.current-menu-item>a',
		        '.edgtf-light-header.edgtf-header-vertical .edgtf-vertical-menu ul li ul li.current_page_item>a',
		        '.edgtf-light-header.edgtf-header-vertical .edgtf-vertical-menu>ul>li.current-menu-ancestor>a',
		        '.edgtf-light-header.edgtf-header-vertical .edgtf-vertical-menu>ul>li.edgtf-active-item>a',
		        '.edgtf-dark-header.edgtf-header-vertical .edgtf-vertical-menu ul li a:hover',
		        '.edgtf-dark-header.edgtf-header-vertical .edgtf-vertical-menu ul li ul li.current-menu-ancestor>a',
		        '.edgtf-dark-header.edgtf-header-vertical .edgtf-vertical-menu ul li ul li.current-menu-item>a',
		        '.edgtf-dark-header.edgtf-header-vertical .edgtf-vertical-menu ul li ul li.current_page_item>a',
		        '.edgtf-dark-header.edgtf-header-vertical .edgtf-vertical-menu>ul>li.current-menu-ancestor>a',
		        '.edgtf-dark-header.edgtf-header-vertical .edgtf-vertical-menu>ul>li.edgtf-active-item>a',
		        '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-search-opener:hover',
		        '.edgtf-light-header .edgtf-top-bar .edgtf-search-opener:hover',
		        '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-search-opener:hover',
		        '.edgtf-dark-header .edgtf-top-bar .edgtf-search-opener:hover',
		        '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-side-menu-button-opener.opened',
		        '.edgtf-light-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-side-menu-button-opener:hover',
		        '.edgtf-light-header .edgtf-top-bar .edgtf-side-menu-button-opener.opened',
		        '.edgtf-light-header .edgtf-top-bar .edgtf-side-menu-button-opener:hover',
		        '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-side-menu-button-opener.opened',
		        '.edgtf-dark-header .edgtf-page-header>div:not(.edgtf-sticky-header):not(.fixed) .edgtf-side-menu-button-opener:hover',
		        '.edgtf-dark-header .edgtf-top-bar .edgtf-side-menu-button-opener.opened',
		        '.edgtf-dark-header .edgtf-top-bar .edgtf-side-menu-button-opener:hover'
	        );

            $background_color_selector = array(
	            'blockquote:before',
	            '.edgtf-st-loader .pulse',
	            '.edgtf-st-loader .double_pulse .double-bounce1',
	            '.edgtf-st-loader .double_pulse .double-bounce2',
	            '.edgtf-st-loader .cube',
	            '.edgtf-st-loader .rotating_cubes .cube1',
	            '.edgtf-st-loader .rotating_cubes .cube2',
	            '.edgtf-st-loader .stripes>div',
	            '.edgtf-st-loader .wave>div',
	            '.edgtf-st-loader .two_rotating_circles .dot1',
	            '.edgtf-st-loader .two_rotating_circles .dot2',
	            '.edgtf-st-loader .five_rotating_circles .container1>div',
	            '.edgtf-st-loader .five_rotating_circles .container2>div',
	            '.edgtf-st-loader .five_rotating_circles .container3>div',
	            '.edgtf-st-loader .atom .ball-1:before',
	            '.edgtf-st-loader .atom .ball-2:before',
	            '.edgtf-st-loader .atom .ball-3:before',
	            '.edgtf-st-loader .atom .ball-4:before',
	            '.edgtf-st-loader .clock .ball:before',
	            '.edgtf-st-loader .mitosis .ball',
	            '.edgtf-st-loader .lines .line1',
	            '.edgtf-st-loader .lines .line2',
	            '.edgtf-st-loader .lines .line3',
	            '.edgtf-st-loader .lines .line4',
	            '.edgtf-st-loader .fussion .ball',
	            '.edgtf-st-loader .fussion .ball-1',
	            '.edgtf-st-loader .fussion .ball-2',
	            '.edgtf-st-loader .fussion .ball-3',
	            '.edgtf-st-loader .fussion .ball-4',
	            '.edgtf-st-loader .wave_circles .ball',
	            '.edgtf-st-loader .pulse_circles .ball',
	            '#submit_comment',
	            '.post-password-form input[type=submit]',
	            'input.wpcf7-form-control.wpcf7-submit',
	            '#edgtf-back-to-top>span',
	            '.widget #wp-calendar td#today',
	            '.widget.widget_tag_cloud a:after',
	            '.edgtf-social-icons-group-widget.edgtf-square-icons .edgtf-social-icon-widget-holder:hover',
	            '.edgtf-social-icons-group-widget.edgtf-square-icons.edgtf-light-skin .edgtf-social-icon-widget-holder:hover',
	            '.edgtf-blog-holder article.format-quote .edgtf-post-text',
	            '.edgtf-blog-holder article.format-audio .edgtf-blog-audio-holder .mejs-container .mejs-controls>.mejs-time-rail .mejs-time-total .mejs-time-current',
	            '.edgtf-blog-holder article.format-audio .edgtf-blog-audio-holder .mejs-container .mejs-controls>a.mejs-horizontal-volume-slider .mejs-horizontal-volume-current',
	            '.edgtf-blog-list-holder .edgtf-bli-inner .edgtf-post-title a:after',
	            'nav.edgtf-fullscreen-menu ul li a:after',
	            '.edgtf-portfolio-list-holder.edgtf-pl-gallery-overlay-2 .edgtf-pli-text-holder',
	            '.edgtf-portfolio-list-holder.edgtf-pl-gallery-overlay .edgtf-pli-text-holder .edgtf-pli-text .edgtf-pli-title:after',
	            '.edgtf-accordion-holder.edgtf-ac-boxed .edgtf-accordion-title.ui-state-active',
	            '.edgtf-accordion-holder.edgtf-ac-boxed .edgtf-accordion-title.ui-state-hover',
	            '.edgtf-btn.edgtf-btn-solid',
	            '.edgtf-btn.edgtf-btn-solid.edgtf-hover-animation.edgtf-btn-white-blue .edgtf-btn-hover-item',
	            '.edgtf-iwt .edgtf-iwt-icon~.edgtf-iwt-content .edgtf-iwt-title-text:after',
	            '.edgtf-icon-shortcode.edgtf-circle',
	            '.edgtf-icon-shortcode.edgtf-dropcaps.edgtf-circle',
	            '.edgtf-icon-shortcode.edgtf-square',
	            '.edgtf-ils-holder.edgtf-ils-type-standard .edgtf-ils-item-title:before',
	            '.edgtf-ils-holder.edgtf-ils-type-slider.edgtf-ils-skin-light .edgtf-ils-item-link:after',
	            '.edgtf-progress-bar .edgtf-pb-content-holder .edgtf-pb-content',
	            '.edgtf-section-title-holder .edgtf-st-separator',
	            '.edgtf-tabs .edgtf-tabs-nav li a:after',
	            '.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-active a',
	            '.edgtf-tabs.edgtf-tabs-simple .edgtf-tabs-nav li.ui-state-active a',
	            '.edgtf-team-holder .edgtf-team-social-wrapper',
	            '.edgtf-video-button-holder.edgtf-vb-predefined-style .edgtf-video-button-play-image .edgtf-video-button-play-inner .edgtf-custom-video-button-play:after'
            );

            $woo_background_color_selector = array();
            if(orkan_edge_is_woocommerce_installed()) {
                $woo_background_color_selector = array(
	                '.woocommerce-page .edgtf-content .wc-forward:not(.added_to_cart):not(.checkout-button)',
	                '.woocommerce-page .edgtf-content a.added_to_cart',
	                '.woocommerce-page .edgtf-content a.button',
	                '.woocommerce-page .edgtf-content button[type=submit]:not(.edgtf-woo-search-widget-button)',
	                '.woocommerce-page .edgtf-content input[type=submit]',
	                'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button)',
	                'div.woocommerce a.added_to_cart',
	                'div.woocommerce a.button',
	                'div.woocommerce button[type=submit]:not(.edgtf-woo-search-widget-button)',
	                'div.woocommerce input[type=submit]',
	                'ul.products>.product .added_to_cart:hover',
	                'ul.products>.product .button:hover',
	                '.edgtf-woo-single-page .woocommerce-tabs ul.tabs>li.active a',
	                '.edgtf-woo-single-page .woocommerce-tabs ul.tabs>li a:after',
	                '.edgtf-shopping-cart-holder .edgtf-header-cart .edgtf-cart-number',
	                '.widget.woocommerce.widget_product_tag_cloud .tagcloud a:after',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-light-skin .added_to_cart:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-light-skin .button:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-dark-skin .added_to_cart:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-dark-skin .button:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .added_to_cart:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .button:hover'
                );
            }

            $background_color_selector = array_merge($background_color_selector, $woo_background_color_selector);
	
	        $background_color_important_selector = array(
		        '.error404 .edgtf-page-not-found .edgtf-btn.edgtf-btn-light-style:hover',
		        '.edgtf-btn.edgtf-btn-solid.edgtf-hover-animation:not(.edgtf-btn-custom-hover-bg):hover',
		        '.edgtf-btn.edgtf-btn-outline:not(.edgtf-btn-custom-hover-bg):hover'
	        );
	        
            $border_color_selector = array(
	            '.edgtf-st-loader .pulse_circles .ball',
	            '.edgtf-btn.edgtf-btn-outline',
	            '.edgtf-tabs.edgtf-tabs-simple .edgtf-tabs-nav',
	            '.edgtf-woo-single-page .woocommerce-tabs ul.tabs'
            );
	        
	        $border_color_important_selector = array(
		        '.error404 .edgtf-page-not-found .edgtf-btn.edgtf-btn-light-style:hover',
		        '.edgtf-btn.edgtf-btn-outline:not(.edgtf-btn-custom-border-hover):hover'
	        );

            echo orkan_edge_dynamic_css($color_selector, array('color' => $first_main_color));
	        echo orkan_edge_dynamic_css($color_important_selector, array('color' => $first_main_color.'!important'));
	        echo orkan_edge_dynamic_css($background_color_selector, array('background-color' => $first_main_color));
	        echo orkan_edge_dynamic_css($background_color_important_selector, array('background-color' => $first_main_color.'!important'));
	        echo orkan_edge_dynamic_css($border_color_selector, array('border-color' => $first_main_color));
	        echo orkan_edge_dynamic_css($border_color_important_selector, array('border-color' => $first_main_color.'!important'));
        }
	
	    $page_background_color = orkan_edge_options()->getOptionValue( 'page_background_color' );
	    if ( ! empty( $page_background_color ) ) {
		    $background_color_selector = array(
			    'body',
			    '.edgtf-content'
		    );
		    echo orkan_edge_dynamic_css( $background_color_selector, array( 'background-color' => $page_background_color ) );
	    }
	
	    $selection_color = orkan_edge_options()->getOptionValue( 'selection_color' );
	    if ( ! empty( $selection_color ) ) {
		    echo orkan_edge_dynamic_css( '::selection', array( 'background' => $selection_color ) );
		    echo orkan_edge_dynamic_css( '::-moz-selection', array( 'background' => $selection_color ) );
	    }
	
	    $preload_background_styles = array();
	
	    if ( orkan_edge_options()->getOptionValue( 'preload_pattern_image' ) !== "" ) {
		    $preload_background_styles['background-image'] = 'url(' . orkan_edge_options()->getOptionValue( 'preload_pattern_image' ) . ') !important';
	    }
	
	    echo orkan_edge_dynamic_css( '.edgtf-preload-background', $preload_background_styles );
    }

    add_action('orkan_edge_style_dynamic', 'orkan_edge_design_styles');
}

if ( ! function_exists( 'orkan_edge_content_styles' ) ) {
	function orkan_edge_content_styles() {
		$content_style = array();
		
		$padding_top = orkan_edge_options()->getOptionValue( 'content_top_padding' );
		if ( $padding_top !== '' ) {
			$content_style['padding-top'] = orkan_edge_filter_px( $padding_top ) . 'px';
		}
		
		$content_selector = array(
			'.edgtf-content .edgtf-content-inner > .edgtf-full-width > .edgtf-full-width-inner',
		);
		
		echo orkan_edge_dynamic_css( $content_selector, $content_style );
		
		$content_style_in_grid = array();
		
		$padding_top_in_grid = orkan_edge_options()->getOptionValue( 'content_top_padding_in_grid' );
		if ( $padding_top_in_grid !== '' ) {
			$content_style_in_grid['padding-top'] = orkan_edge_filter_px( $padding_top_in_grid ) . 'px';
		}
		
		$content_selector_in_grid = array(
			'.edgtf-content .edgtf-content-inner > .edgtf-container > .edgtf-container-inner',
		);
		
		echo orkan_edge_dynamic_css( $content_selector_in_grid, $content_style_in_grid );
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_content_styles' );
}

if ( ! function_exists( 'orkan_edge_h1_styles' ) ) {
	function orkan_edge_h1_styles() {
		$margin_top    = orkan_edge_options()->getOptionValue( 'h1_margin_top' );
		$margin_bottom = orkan_edge_options()->getOptionValue( 'h1_margin_bottom' );
		
		$item_styles = orkan_edge_get_typography_styles( 'h1' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = orkan_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = orkan_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h1'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo orkan_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_h1_styles' );
}

if ( ! function_exists( 'orkan_edge_h2_styles' ) ) {
	function orkan_edge_h2_styles() {
		$margin_top    = orkan_edge_options()->getOptionValue( 'h2_margin_top' );
		$margin_bottom = orkan_edge_options()->getOptionValue( 'h2_margin_bottom' );
		
		$item_styles = orkan_edge_get_typography_styles( 'h2' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = orkan_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = orkan_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h2'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo orkan_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_h2_styles' );
}

if ( ! function_exists( 'orkan_edge_h3_styles' ) ) {
	function orkan_edge_h3_styles() {
		$margin_top    = orkan_edge_options()->getOptionValue( 'h3_margin_top' );
		$margin_bottom = orkan_edge_options()->getOptionValue( 'h3_margin_bottom' );
		
		$item_styles = orkan_edge_get_typography_styles( 'h3' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = orkan_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = orkan_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h3'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo orkan_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_h3_styles' );
}

if ( ! function_exists( 'orkan_edge_h4_styles' ) ) {
	function orkan_edge_h4_styles() {
		$margin_top    = orkan_edge_options()->getOptionValue( 'h4_margin_top' );
		$margin_bottom = orkan_edge_options()->getOptionValue( 'h4_margin_bottom' );
		
		$item_styles = orkan_edge_get_typography_styles( 'h4' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = orkan_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = orkan_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h4'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo orkan_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_h4_styles' );
}

if ( ! function_exists( 'orkan_edge_h5_styles' ) ) {
	function orkan_edge_h5_styles() {
		$margin_top    = orkan_edge_options()->getOptionValue( 'h5_margin_top' );
		$margin_bottom = orkan_edge_options()->getOptionValue( 'h5_margin_bottom' );
		
		$item_styles = orkan_edge_get_typography_styles( 'h5' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = orkan_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = orkan_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h5'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo orkan_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_h5_styles' );
}

if ( ! function_exists( 'orkan_edge_h6_styles' ) ) {
	function orkan_edge_h6_styles() {
		$margin_top    = orkan_edge_options()->getOptionValue( 'h6_margin_top' );
		$margin_bottom = orkan_edge_options()->getOptionValue( 'h6_margin_bottom' );
		
		$item_styles = orkan_edge_get_typography_styles( 'h6' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = orkan_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = orkan_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h6'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo orkan_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_h6_styles' );
}

if ( ! function_exists( 'orkan_edge_text_styles' ) ) {
	function orkan_edge_text_styles() {
		$item_styles = orkan_edge_get_typography_styles( 'text' );
		
		$item_selector = array(
			'p'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo orkan_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_text_styles' );
}

if ( ! function_exists( 'orkan_edge_link_styles' ) ) {
	function orkan_edge_link_styles() {
		$link_styles      = array();
		$link_color       = orkan_edge_options()->getOptionValue( 'link_color' );
		$link_font_style  = orkan_edge_options()->getOptionValue( 'link_fontstyle' );
		$link_font_weight = orkan_edge_options()->getOptionValue( 'link_fontweight' );
		$link_decoration  = orkan_edge_options()->getOptionValue( 'link_fontdecoration' );
		
		if ( ! empty( $link_color ) ) {
			$link_styles['color'] = $link_color;
		}
		if ( ! empty( $link_font_style ) ) {
			$link_styles['font-style'] = $link_font_style;
		}
		if ( ! empty( $link_font_weight ) ) {
			$link_styles['font-weight'] = $link_font_weight;
		}
		if ( ! empty( $link_decoration ) ) {
			$link_styles['text-decoration'] = $link_decoration;
		}
		
		$link_selector = array(
			'a',
			'p a'
		);
		
		if ( ! empty( $link_styles ) ) {
			echo orkan_edge_dynamic_css( $link_selector, $link_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_link_styles' );
}

if ( ! function_exists( 'orkan_edge_link_hover_styles' ) ) {
	function orkan_edge_link_hover_styles() {
		$link_hover_styles     = array();
		$link_hover_color      = orkan_edge_options()->getOptionValue( 'link_hovercolor' );
		$link_hover_decoration = orkan_edge_options()->getOptionValue( 'link_hover_fontdecoration' );
		
		if ( ! empty( $link_hover_color ) ) {
			$link_hover_styles['color'] = $link_hover_color;
		}
		if ( ! empty( $link_hover_decoration ) ) {
			$link_hover_styles['text-decoration'] = $link_hover_decoration;
		}
		
		$link_hover_selector = array(
			'a:hover',
			'p a:hover'
		);
		
		if ( ! empty( $link_hover_styles ) ) {
			echo orkan_edge_dynamic_css( $link_hover_selector, $link_hover_styles );
		}
		
		$link_heading_hover_styles = array();
		
		if ( ! empty( $link_hover_color ) ) {
			$link_heading_hover_styles['color'] = $link_hover_color;
		}
		
		$link_heading_hover_selector = array(
			'h1 a:hover',
			'h2 a:hover',
			'h3 a:hover',
			'h4 a:hover',
			'h5 a:hover',
			'h6 a:hover'
		);
		
		if ( ! empty( $link_heading_hover_styles ) ) {
			echo orkan_edge_dynamic_css( $link_heading_hover_selector, $link_heading_hover_styles );
		}
	}
	
	add_action( 'orkan_edge_style_dynamic', 'orkan_edge_link_hover_styles' );
}

if ( ! function_exists( 'orkan_edge_smooth_page_transition_styles' ) ) {
	function orkan_edge_smooth_page_transition_styles( $style ) {
		$id            = orkan_edge_get_page_id();
		$loader_style  = array();
		$current_style = '';
		
		$background_color = orkan_edge_get_meta_field_intersect( 'smooth_pt_bgnd_color', $id );
		if ( ! empty( $background_color ) ) {
			$loader_style['background-color'] = $background_color;
		}
		
		$loader_selector = array(
			'.edgtf-smooth-transition-loader'
		);
		
		if ( ! empty( $loader_style ) ) {
			$current_style .= orkan_edge_dynamic_css( $loader_selector, $loader_style );
		}
		
		$spinner_style = array();
		$spinner_color = orkan_edge_get_meta_field_intersect( 'smooth_pt_spinner_color', $id );
		if ( ! empty( $spinner_color ) ) {
			$spinner_style['background-color'] = $spinner_color;
		}
		
		$spinner_selectors = array(
			'.edgtf-st-loader .edgtf-rotate-circles > div',
			'.edgtf-st-loader .pulse',
			'.edgtf-st-loader .double_pulse .double-bounce1',
			'.edgtf-st-loader .double_pulse .double-bounce2',
			'.edgtf-st-loader .cube',
			'.edgtf-st-loader .rotating_cubes .cube1',
			'.edgtf-st-loader .rotating_cubes .cube2',
			'.edgtf-st-loader .stripes > div',
			'.edgtf-st-loader .wave > div',
			'.edgtf-st-loader .two_rotating_circles .dot1',
			'.edgtf-st-loader .two_rotating_circles .dot2',
			'.edgtf-st-loader .five_rotating_circles .container1 > div',
			'.edgtf-st-loader .five_rotating_circles .container2 > div',
			'.edgtf-st-loader .five_rotating_circles .container3 > div',
			'.edgtf-st-loader .atom .ball-1:before',
			'.edgtf-st-loader .atom .ball-2:before',
			'.edgtf-st-loader .atom .ball-3:before',
			'.edgtf-st-loader .atom .ball-4:before',
			'.edgtf-st-loader .clock .ball:before',
			'.edgtf-st-loader .mitosis .ball',
			'.edgtf-st-loader .lines .line1',
			'.edgtf-st-loader .lines .line2',
			'.edgtf-st-loader .lines .line3',
			'.edgtf-st-loader .lines .line4',
			'.edgtf-st-loader .fussion .ball',
			'.edgtf-st-loader .fussion .ball-1',
			'.edgtf-st-loader .fussion .ball-2',
			'.edgtf-st-loader .fussion .ball-3',
			'.edgtf-st-loader .fussion .ball-4',
			'.edgtf-st-loader .wave_circles .ball',
			'.edgtf-st-loader .pulse_circles .ball'
		);
		
		if ( ! empty( $spinner_style ) ) {
			$current_style .= orkan_edge_dynamic_css( $spinner_selectors, $spinner_style );
		}
		
		$current_style = $current_style . $style;
		
		return $current_style;
	}
	
	add_filter( 'orkan_edge_add_page_custom_style', 'orkan_edge_smooth_page_transition_styles' );
}