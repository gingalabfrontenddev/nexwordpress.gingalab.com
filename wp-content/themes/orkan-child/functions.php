<?php

/*** Child Theme Function  ***/

pll_register_string("goyaves", "By");
pll_register_string("goyaves", "Realized by");
pll_register_string("goyaves", "Read more");

function orkan_edge_child_theme_enqueue_scripts() {

	$parent_style = 'orkan_edge_default_style';

	wp_enqueue_style('orkan_edge_child_style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));

	wp_enqueue_script( 'goyavesjs', get_stylesheet_directory_uri() . '/custom.js', array( 'jquery' ), false, true );
}

add_action( 'wp_enqueue_scripts', 'orkan_edge_child_theme_enqueue_scripts' );
