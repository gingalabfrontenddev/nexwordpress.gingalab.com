<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="edgtf-post-content">
        <div class="edgtf-post-heading">
            <?php orkan_edge_get_module_template_part('templates/parts/media', 'blog', $post_format, $part_params); ?>
        </div>
        <div class="edgtf-post-text">
            <div class="edgtf-post-text-inner">
                <div class="edgtf-post-info-standard">
                    <?php orkan_edge_get_module_template_part('templates/parts/post-info/category', 'blog', '', $part_params); ?>
                    <?php //orkan_edge_get_module_template_part('templates/parts/post-info/date', 'blog', '', $part_params); ?>
                    <?php orkan_edge_get_module_template_part('templates/parts/post-info/tags', 'blog', '', $part_params); ?>
                </div>
                <div class="edgtf-post-text-main">
                    <?php orkan_edge_get_module_template_part('templates/parts/title', 'blog', '', $part_params); ?>
		    <?php if (get_field("movie_author")) { ?>
			<h3>
				<?php echo pll__("Realized by"); ?>
				<?php echo get_field("movie_author"); ?>
				<?php if (get_field("movie_context")) { ?> - <?php echo get_field("movie_context"); ?><?php } ?>
			</h3>
		    <?php } ?>
                    <?php the_content(); ?>
                    <?php do_action('orkan_edge_single_link_pages'); ?>
                </div>
                <div class="edgtf-post-info-standard-bottom">
	                <?php orkan_edge_get_module_template_part('templates/parts/post-info/author', 'blog', '', $part_params); ?>
	                <?php orkan_edge_get_module_template_part('templates/parts/post-info/comments', 'blog', '', $part_params); ?>
	                <?php orkan_edge_get_module_template_part('templates/parts/post-info/like', 'blog', '', $part_params); ?>
                    <?php orkan_edge_get_module_template_part('templates/parts/post-info/share', 'blog', '', $part_params); ?>
                </div>
            </div>
        </div>
    </div>
</article>
